<?php

namespace Drupal\new_module\Controller;

use Drupal\new_module\MyService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines HelloController class, just to write some message.
 */
class HelloController extends \Drupal\Core\Controller\ControllerBase
{
  protected $greeting;

  /**
   * @param MyService $myService
   */
  public function __construct(MyService $myService) {
    $this->greeting = $myService;
  }

  /**
   * return render array
   */
  public function sayHello() {
    return[
      '#theme' => 'custom_theme',
      '#message' => $this->greeting->saySmth(),
    ];
  }

  /**
   * @param ContainerInterface $container
   * @return HelloController|static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('drupal_service.custom')
    );

  }
}
