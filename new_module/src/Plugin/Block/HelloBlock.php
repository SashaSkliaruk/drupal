<?php

namespace Drupal\new_module\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello new block' Block.
 *
 * @Block(
 *   id = "hello_new_block",
 *   admin_label = @Translation("Hello new block"),
 *   category = @Translation("Hello new block"),
 * )
 */
class HelloBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('system.site');
    $message = $config->get('name');
    return [
      '#markup' => 'hi from block, u r on the ' . $message . ' website !'
    ];
  }
}
