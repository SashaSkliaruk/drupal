<?php

namespace Drupal\new_module;

use Drupal\Core\Session\AccountProxy;

/**
 * MyService is a simple example of service.
 */

class MyService {

  private $currentUser;

  /**
   * @param AccountProxy $current_user
   */
  public function __construct(AccountProxy $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * @return string
   */
  public function saySmth() {
    return 'hello ' . $this->currentUser->getDisplayName();
  }
}
